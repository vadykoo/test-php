<?php
require 'vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// add  ftp://, ftps://, http:// https://
function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}
if(!empty($_POST['URL'])) {
    $filename = addhttp($_POST['URL']) . '/robots.txt'; // ссылка на файл
    $headers = @get_headers($filename);
    $code = substr($headers[0], 9, 3);
    $robotstxt = @file($filename);
//print_r($robotstxt);
    if($code == 200 or 301 && !empty($robotstxt)) {
        $status[0] = 'Ок';
        $status[5] = 'Ок';
        //----------------------------------------------------проверка на количество хостов и sitemap
        $hostcount = 0;
        $sitemapcount = 0;
        foreach ($robotstxt as $line) {
            if (strpos($line, 'Sitemap') !== false)
                $sitemapcount++;
            if (strpos($line, 'Host') !== false)
                $hostcount++;
        }

        if ($hostcount > 0) {
            $status[1] = 'Ок';
            if ($hostcount == 1)
                $status[2] = 'Ок';
            else
                $status[2] = 'Ошибка';

        } else {
            $status[1] = 'Ошибка';
            $status[2] = 'Ошибка';
        }

        if ($sitemapcount > 0) {
            $status[4] = 'Ок';
        }
        else{
            $status[4] = 'Ошибка';
        }
        file_put_contents("Tmpfilerobots.txt", $robotstxt);
        $size = filesize('Tmpfilerobots.txt');

        //--------------------------------------------------------------проверка размера файла
        if($size < 32000){
            $status[3] = 'Ок';
        }
        else{
            $status[3] = 'Ошибка';
        }
    }
    else {
        $status[0] = 'Ошибка';
        $status[1] = 'Ошибка';
        $status[2] = 'Ошибка';
        $status[3] = 'Ошибка';
        $status[4] = 'Ошибка';
        $status[5] = 'Ошибка';
    }

    include 'vars.php';

//------------------------------------------------генерация таблицы Excel
    $inputFileName = 'template.xlsx';
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
    $sheet = $spreadsheet->getActiveSheet();
    $a=0;
    $t=0;
    foreach($qw as $q) {
        $a=$a+3;
        $sheet->setCellValue('C'.$a, $status[$t]);
        $sheet->setCellValue('E'.$a, $situation[$t]);
        $sheet->setCellValue('E'.($a+1), $recommendations[$t]);
        $t++;
    }

    $writer = new Xlsx($spreadsheet);
    $nametab = rand(0001, 9999);
    $writer->save('./xlsx/'.$nametab.'.xlsx');

    $k = 0;

    include 'result.php';
}
else {
    echo 'неверный сайт';
}
?>
