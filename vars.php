<?php
$numbers = [1,6,8,10,11,12];
$i = 0;
$qw[$i] = 'Проверка наличия файла robots.txt';
if($status[$i] == 'Ок') {
$situation[$i] = 'Файл robots.txt присутствует';
$recommendations[$i] = 'Доработки не требуются';}
else {
$situation[$i] = 'Файл robots.txt отсутствует';
$recommendations[$i] = 'Программист: Создать файл robots.txt и разместить его на сайте.';}

$i++;
$qw[$i] = 'Проверка указания директивы Host';
if($status[$i] == 'Ок') {
$situation[$i] = 'Директива Host указана';
$recommendations[$i] = 'Доработки не требуются';}
else {
$situation[$i] = 'В файле robots.txt не указана директива Host';
$recommendations[$i] = 'Программист: Для того, чтобы поисковые системы знали, какая версия сайта является основных зеркалом, необходимо прописать адрес основного зеркала в директиве Host. В данный момент это не прописано. Необходимо добавить в файл robots.txt директиву Host. Директива Host задётся в файле 1 раз, после всех правил.';}

$i++;
$qw[$i] = 'Проверка количества директив Host, прописанных в файле';
if($status[$i] == 'Ок') {
$situation[$i] = 'В файле прописана 1 директива Host';
$recommendations[$i] = 'Доработки не требуются';}
else{
$situation[$i] = 'В файле не прописано или прописано несколько директив Host';
$recommendations[$i] = 'Программист: Директива Host должна быть указана в файле толоко 1 раз. Необходимо удалить все дополнительные директивы Host и оставить только 1, корректную и соответствующую основному зеркалу сайта';}

$i++;
$qw[$i] = 'Проверка размера файла robots.txt';
if($status[$i] == 'Ок') {
$situation[$i] = 'Размер файла robots.txt составляет '.$size.'байт, что находится в пределах допустимой нормы';
$recommendations[$i] = 'Доработки не требуются';}
else{
$situation[$i] = 'Размера файла robots.txt составляет '.$size.'байт, что превышает допустимую норму';
$recommendations[$i] = 'Программист: Максимально допустимый размер файла robots.txt составляем 32 кб. Необходимо отредактировть файл robots.txt таким образом, чтобы его размер не превышал 32 Кб';}

$i++;
$qw[$i] = 'Проверка указания директивы Sitemap';
if($status[$i] == 'Ок') {
$situation[$i] = 'Директива Sitemap указана';
$recommendations[$i] = 'Доработки не требуются';}
else{
$situation[$i] = 'В файле robots.txt не указана директива Sitemap';
$recommendations[$i] = 'Программист: Добавить в файл robots.txt директиву Sitemap';}

$i++;
$qw[$i] = 'Проверка кода ответа сервера для файла robots.txt';
if($status[$i] == 'Ок') {
$situation[$i] = 'Файл robots.txt отдаёт код ответа сервера '  . $code;
$recommendations[$i] = 'Доработки не требуются';}
else{
$situation[$i] = 'При обращении к файлу robots.txt сервер возвращает код ответа ' . $code;
$recommendations[$i] = 'Программист: Файл robots.txt должны отдавать код ответа 200, иначе файл не будет обрабатываться. Необходимо настроить сайт таким образом, чтобы при обращении к файлу robots.txt сервер возвращает код ответа 200';}
?>
